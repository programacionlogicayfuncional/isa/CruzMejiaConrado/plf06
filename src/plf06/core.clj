(ns plf06.core
  (:gen-class))

(def datas (vector "aábcdeéfghiíjklmnñoópqrstuúüvwxyz"
                   "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ"
                   "01234!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~56789"))

(def alfabeto [\A \B \C \D \E \F \G \H \I \J \K \L \M \N \Ñ \O \P \Q \R \S \T \U \V \W \X \Y \Z])

(defn tabla-sustitución
  [d]
  (into {} (map vector alfabeto
                (concat (drop d alfabeto)
                        (take d alfabeto)))))

(defn sustitución
  [tabla c]
  (let [v (tabla c)]
    (if (char? v) v c)))

(defn codificar
  [d s]
  (let [tabla (tabla-sustitución d)]
    (apply str (map (fn [c] (sustitución tabla c)) s))))

(defn -main
  [& args]
  (if (empty? args)
    (println "Debe ingresar por lo menos un caracter. \nSi es un caracter especial puede utilizar la diagonal inversa (\\) para escapar")
    (println (codificar (apply str args)))))

;Test
;(encrypt (apply str "Canción" "#72"))
;(encrypt (apply str "Canción" "#" "72"))
;;(encrypt (apply str "Canc" "ión" "#" "72"))